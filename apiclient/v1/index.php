<?php

/* 
 * Uso de la api/v1
 */
require_once 'CurlRequest.php';
$curl = new CurlRequest;

//ejemplo de peticion GET
//$url= "http://localhost/tema6/rest/java";
$url= "http://localhost/tema6/api/v1/users/1";
$curl->sendGet($url);

//ejemplo de peticion POST
$url= "http://localhost/tema6/api/v1/users";
$data['id']="50";
$data['nombre']='Fulanito';
$data['password']='mipassword';
$curl->sendPost($url,$data);

//ejemplo de peticion PUT
$url= "http://localhost/tema6/api/v1/users/1";
$data['id']="1";
$data['nombre']='Menganito';
$data['password']='SUpassword';
$curl->sendPut($url,$data);

//ejemplo de peticion DELETE
$url= "http://localhost/tema6/api/v1/users/1";
$curl->sendDelete($url,$data);
